package alphabetsoup.utils;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import alphabetsoup.model.GameGrid;

@RunWith(JUnit4.class)
public class AlphabetSoupUtilsTest {

	private final String fileName1 = "target/test-classes/testSample1.txt";

	private GameGrid gameGrid;

	@Before
	public void setup() {
		gameGrid = new GameGrid();
	} // ends setup

	/**
	 * Tests normal operating situation
	 */
	@Test
	public void testProcessFile() {
		gameGrid = AlphabetSoupUtils.processFile(fileName1);
		assertTrue(gameGrid.getRow() == 3);
		assertTrue(gameGrid.getColumn() == 3);
		assertTrue(gameGrid.getWordList().size() == 2);
		assertTrue(gameGrid.getGridList().size() == 3);
	} // ends testProcessFile

	/**
	 * Tests null file name
	 */
	@Test(expected = NullPointerException.class)
	public void testProcessFileNullFileName() {
		gameGrid = AlphabetSoupUtils.processFile(null);
	} // ends testProcessFileNullFileName

	@Test
	public void testProcessFileEmptyFileName() {
		gameGrid = AlphabetSoupUtils.processFile("");
	} // ends
} // AlphabetSoupUtilsTest
