package alphabetsoup.service;

import org.junit.Test;

/**
 * Unit tests for AlphabetSoupService Since most of the methods are private,
 * tests are closer to integration testing Private methods can be tested by
 * reflection, however, this will not be done here for brevity
 */
public class AlphabetSoupServiceTest {

	private final String fileName1 = "target/test-classes/testSample1.txt";

	private AlphabetSoupService service = new AlphabetSoupService();

	@Test
	public void testLetsPlay() {
		service.letsPlay(fileName1);
	} // ends letsPlay

} // ends AlphabetSoupServiceTest
