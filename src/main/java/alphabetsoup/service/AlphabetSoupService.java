package alphabetsoup.service;

import java.util.ArrayList;
import java.util.List;

import alphabetsoup.model.GameGrid;
import alphabetsoup.utils.AlphabetSoupUtils;

/**
 * Service managerowforAlphabetSpoup game
 */
public class AlphabetSoupService {

	public void letsPlay(String fileName) {
		GameGrid gameGrid = new GameGrid();
		List<String> answerList = new ArrayList<String>();

		// create the game from the file
		gameGrid = AlphabetSoupUtils.processFile(fileName);

		// play the game
		answerList = findTheWords(gameGrid);

		// print the answers
		showOff(answerList);

	} // ends letsPlay

	/**
	 * Goes through each word and loops through the rows and then columns to see if
	 * the letter matches any neighbors (up, up right, right, down right, down, down
	 * left, left, up left)
	 * 
	 * @param gameGrid
	 * @return the words and beginning and end coordinates
	 */
	private List<String> findTheWords(GameGrid gameGrid) {
		List<String> retVal = new ArrayList<String>();
		char[][] letterPoint = new char[gameGrid.getRow()][gameGrid.getColumn()];

		// fill up the grid of letters
		int x = 0;
		for (String rowStr : gameGrid.getGridList()) {
			for (int y = 0; y < rowStr.length(); y++) {
				letterPoint[x][y] = rowStr.charAt(y);
			} // ends for length (columns)
			x++;
		} // ends for each row

		// loop through the words
		for (String currWord : gameGrid.getWordList()) {
			// System.out.println("Searching for " + currWord);
			for (int row = 0; row < gameGrid.getRow(); row++) {
				for (int col = 0; col < gameGrid.getColumn(); col++) {
					// check first character see if it matches
					//// System.out.println("");
					//// System.out.println("checking '" + letterPoint[row][col] + "' != " +
					// currWord.charAt(0));
					if (letterPoint[row][col] != currWord.charAt(0)) {
						continue;
					} // ends if for first character match

					// circle around point and look for matches
					// going clockwise starting at 12:00

					// up (row-1)
					//// System.out.println("up (row-1)");
					if (row - currWord.length() >= -1) {
						for (int x1 = 0; x1 < currWord.length(); x1++) {
							if (letterPoint[row - x1][col] == currWord.charAt(x1)) {
								if (x1 == currWord.length() - 1) {
									retVal.add(currWord + " " + row + ":" + col + " " + (row - currWord.length() + 1)
											+ ":" + col);
								} // ends if for found
							} else {
								break;
							} // ends if else for possible find or not so break
						} // ends for loop for word length
					} // ends if for not enough letters for the direction

					// up right (row-1, col+1)
					//// System.out.println("up right (row-1, col+1)");
					if (row >= currWord.length() - 1 && col + currWord.length() <= gameGrid.getColumn()) {
						for (int y = 0; y < currWord.length(); y++) {
							if (letterPoint[row - y][col + y] == currWord.charAt(y)) {
								if (y == currWord.length() - 1) {
									retVal.add(currWord + " " + row + ":" + col + " " + (row - currWord.length() + 1)
											+ ":" + (col + currWord.length() - 1));
								} // ends if for found
							} else {
								break;
							} // ends if else for possible find or not so break
						} // ends for loop for word length
					} // ends if for not enough letters for the direction

					// right (col+1)
					//// System.out.println("right (col+1)");
					if (col + currWord.length() <= gameGrid.getColumn()) {
						for (int z = 0; z < currWord.length(); z++) {
							if (letterPoint[row][col + z] == currWord.charAt(z)) {
								if (z == currWord.length() - 1) {
									retVal.add(currWord + " " + row + ":" + col + " " + row + ":"
											+ (col + currWord.length() - 1));
								} // ends if for found
							} else {
								break;
							} // ends if else for possible find or not so break
						} // ends for loop for word length
					} // ends if for not enough letters for the direction

					// bottom right (row+1, col+1)
					//// System.out.println("bottom right (row+1, col+1)");
					if (row + currWord.length() <= gameGrid.getRow()
							&& col + currWord.length() <= gameGrid.getColumn()) {
						for (int i = 0; i < currWord.length(); i++) {
							if (letterPoint[row + i][col + i] == currWord.charAt(i)) {
								if (i == currWord.length() - 1) {
									retVal.add(currWord + " " + row + ":" + col + " " + (row + currWord.length() - 1)
											+ ":" + (col + currWord.length() - 1));
								} // ends if for found
							} else {
								break;
							} // ends if else for possible find or not so break
						} // ends for loop for word length
					} // ends if for not enough letters for the direction

					// bottom (row+1)
					//// System.out.println("bottom right (row+1, col+1)");
					if (row + currWord.length() <= gameGrid.getRow()) {
						for (int j = 0; j < currWord.length(); j++) {
							if (letterPoint[row + j][col] == currWord.charAt(j)) {
								if (j == currWord.length() - 1) {
									retVal.add(currWord + " " + row + ":" + col + " " + (row + currWord.length() - 1)
											+ ":" + col);
								} // ends if for found
							} else {
								break;
							} // ends if else for possible find or not so break
						} // ends for loop for word length
					} // ends if for not enough letters for the direction

					// bottom left (row+1, col-1)
					//// System.out.println("bottom left (row+1, col-1)");
					if (row + currWord.length() <= gameGrid.getRow() && col - currWord.length() >= -1) {
						for (int a = 0; a < currWord.length(); a++) {
							if (letterPoint[row + a][col - a] == currWord.charAt(a)) {
								if (a == currWord.length() - 1) {
									retVal.add(currWord + " " + row + ":" + col + " " + (row + currWord.length() - 1)
											+ ":" + (col - currWord.length() + 1));
								} // ends if for found
							} else {
								break;
							} // ends if else for possible find or not so break
						} // ends for loop for word length
					} // ends if for not enough letters for the direction

					// left (row-1)
					//// System.out.println("left (row-1)");
					if (col - currWord.length() >= -1) {
						for (int b = 0; b < currWord.length(); b++) {
							if (letterPoint[row][col - b] == currWord.charAt(b)) {
								if (b == currWord.length() - 1) {
									retVal.add(currWord + " " + row + ":" + col + " " + row + ":"
											+ (col - currWord.length() + 1));
								} // ends if for found
							} else {
								break;
							} // ends if else for possible find or not so break
						} // ends for loop for word length
					} // ends if for not enough letters for the direction

					// up left (row-1, col-1)
					//// System.out.println("left (row-1)");
					if (row >= currWord.length() - 1 && col >= currWord.length() - 1) {
						for (int c = 0; c < currWord.length(); c++) {
							if (letterPoint[row - c][col - c] == currWord.charAt(c)) {
								if (c == currWord.length() - 1) {
									retVal.add(currWord + " " + row + ":" + col + " " + (row - currWord.length() + 1)
											+ ":" + (col - currWord.length() + 1));
								} // ends if for found
							} else {
								break;
							} // ends if else for possible find or not so break
						} // ends for loop for word length
					} // ends if for not enough letters for the direction
				} // ends for loop for column
			} // ends for loop for row
		} // ends for each word in wordList

		// System.out.println("retVal.size=" + retVal.size());
		return retVal;
	} // ends findTheWords

	private void showOff(List<String> answerList) {
		for (String answerStr : answerList) {
			System.out.println(answerStr);
		} // ends for each answerList
	} // ends showOff

} // ends AlphabetSoupService
