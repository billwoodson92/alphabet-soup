/**
 * 
 */
package alphabetsoup.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object model
 */

public class GameGrid {

	public List<Integer> gridSize;
	public List<String> gridList;
	public List<String> wordList;

	public GameGrid() {
		gridSize = new ArrayList<Integer>();
		gridList = new ArrayList<String>();
		wordList = new ArrayList<String>();
	} // ends default constructor

	/**
	 * @return the gridSize
	 */
	public List<Integer> getGridSize() {
		return gridSize;
	}

	/**
	 * @param gridSize the gridSize to set
	 */
	public void setGridSize(List<Integer> gridSize) {
		this.gridSize = gridSize;
	}

	/**
	 * @return the gridList
	 */
	public List<String> getGridList() {
		return gridList;
	}

	/**
	 * @param gridList the gridList to set
	 */
	public void setGridList(List<String> gridList) {
		this.gridList = gridList;
	}

	/**
	 * @return the wordList
	 */
	public List<String> getWordList() {
		return wordList;
	}

	/**
	 * @param wordList the wordList to set
	 */
	public void setWordList(List<String> wordList) {
		this.wordList = wordList;
	}

	/**
	 * @return the row
	 */
	public int getRow() {
		return getGridSize().get(0);
	} // ends getRow

	/**
	 * @return the column
	 */
	public int getColumn() {
		return getGridSize().get(1);
	}
} // ends GameGrid
