/**
 * 
 */
package alphabetsoup.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import alphabetsoup.model.GameGrid;

/**
 * Utilities class for convenience methods
 */
public class AlphabetSoupUtils {

	/**
	 * Processes the wordsearch.txt file, pulling out the data based on <br>
	 * the format provided in the readme.md<br>
	 * <br>
	 * <b>NOTE:</b> No error checking is performed. The data is assumed to be
	 * formatted correctly
	 * 
	 * @param file the data input file
	 * @return 2D array with [0] being the alphabet matrix rows and [1] being the
	 *         words to search for
	 */
	public static GameGrid processFile(String fileName) {
		GameGrid retVal = new GameGrid();
		ArrayList<String> allDataList = new ArrayList<String>();
		ArrayList<String> rowList = new ArrayList<String>();
		ArrayList<String> wordList = new ArrayList<String>();

		// Read in the file
		// System.out.println("fileName = " + fileName);
		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = br.readLine()) != null) {
				allDataList.add(line);
			} // ends while to parse file
				// System.out.println("allDataList.size = " + allDataList.size());

			// Get the grid size
			String[] gridSize = allDataList.get(0).split("x");
			List<Integer> gridSizeList = new ArrayList<Integer>();
			gridSizeList.add(Integer.valueOf(gridSize[0]));
			gridSizeList.add(Integer.valueOf(gridSize[1]));
			retVal.setGridSize(gridSizeList);

			// pull out the rows and remove spaces
			for (int i = 1; i <= retVal.getRow(); i++) {
				// System.out.println("Adding to rowList: " + allDataList.get(i).replaceAll(" ",
				// "").toString());
				rowList.add(allDataList.get(i).replaceAll(" ", ""));
			} // ends for loop for rows

			// pull out the search words
			for (int j = retVal.getRow() + 1; j <= allDataList.size() - 1; j++) {
				// System.out.println("Adding to wordList: " + allDataList.get(j).replaceAll("
				// ", "").toString());
				wordList.add(allDataList.get(j).replaceAll(" ", ""));
			} // ends for loop for search words

			// Put together the return value
			// System.out.println("rowList.size = " + rowList.size());
			retVal.setGridList(rowList);
			// System.out.println("wordList.size = " + wordList.size());
			retVal.setWordList(wordList);
		} catch (IOException e) {
			// e.printStackTrace();
			// Log the error
		} finally {
			// Gotta close the buffer reader
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// e.printStackTrace();
					// Log the errors
				} // ends try catch for io exception
			} // ends if for not null
		} // ends try catch

		return retVal;
	} // ends processFile

} // ends AlphabetSoupUtils
