/**
 * 
 */
package alphabetsoup;

import alphabetsoup.service.AlphabetSoupService;

/**
 * Main class for alphabetsoup application
 */
public class App {

	/**
	 * Apps main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length > 0) {
			AlphabetSoupService service = new AlphabetSoupService();

			service.letsPlay(args[0]);
		} else {
			System.out.println("Please provide a file to process");
		} // ends if/else for no file
	} // ends main
} // ends App
